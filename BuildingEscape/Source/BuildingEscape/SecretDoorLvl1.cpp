// Copyright Sam Collier 2018

#include "SecretDoorLvl1.h"


// Sets default values for this component's properties
USecretDoorLvl1::USecretDoorLvl1()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USecretDoorLvl1::BeginPlay()
{
	Super::BeginPlay();
	ActorThatOpens = GetOwner();
	
}


// Called every frame
void USecretDoorLvl1::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetTotalMassOfActorsOnPlate() == PressurePlateThreshold)
	{
		OnOpen.Broadcast();
	}
	else
	{
		OnClose.Broadcast();
	}

}

float USecretDoorLvl1::GetTotalMassOfActorsOnPlate()
{
	float TotalMass = 0.f;

	// Find all the overlapping actors
	TArray<AActor*> OverlappingActors;
	if (!PressurePlate) { return TotalMass; }
	PressurePlate->GetOverlappingActors(OUT OverlappingActors);
	// Iterate through them by adding their masses
	for (const auto &Actors : OverlappingActors)
	{
		TotalMass += Actors->FindComponentByClass<UPrimitiveComponent>()->GetMass();
		UE_LOG(LogTemp, Warning, TEXT("%s is in the box"), *Actors->GetName())
	}

	return TotalMass;
}

