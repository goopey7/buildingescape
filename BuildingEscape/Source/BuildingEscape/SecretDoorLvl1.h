// Copyright Sam Collier 2018

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "Runtime/Core/Public/Containers/Array.h"
#include "SecretDoorLvl1.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSecretDoorEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API USecretDoorLvl1 : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USecretDoorLvl1();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(EditAnywhere)
	ATriggerVolume *PressurePlate = nullptr;
	
	AActor *ActorThatOpens = nullptr; // Remember Pawn Inherits Actor
	float GetTotalMassOfActorsOnPlate();
	
	UPROPERTY(BlueprintAssignable)
	FSecretDoorEvent OnOpen;

	UPROPERTY(BlueprintAssignable)
	FSecretDoorEvent OnClose;

	UPROPERTY(EditAnywhere)
	float PressurePlateThreshold = 80.f;
};
