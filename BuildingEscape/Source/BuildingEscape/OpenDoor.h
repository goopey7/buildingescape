// Copyright Sam Collier 2018

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "Runtime/Core/Public/Containers/Array.h"
#include "OpenDoor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable)
	FDoorEvent OnOpen;

private:
	UPROPERTY(BlueprintAssignable)
	FDoorEvent OnClose;

	UPROPERTY(EditAnywhere)
	ATriggerVolume *PressurePlate = nullptr;
	
	UPROPERTY(EditAnywhere)
	// in kg
	float PressurePlateThreshold = 45.f;

	AActor *Owner = nullptr; // The owning door

	// returns total mass in kg
	float GetTotalMassOfActorsOnPlate();



};
